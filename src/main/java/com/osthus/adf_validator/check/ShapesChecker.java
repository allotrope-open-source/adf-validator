/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.adf_validator.check;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.topbraid.shacl.vocabulary.SH;

public class ShapesChecker
{

	public static void check(Model shapesModel, Model instanceModel) throws FileNotFoundException
	{
		ShaclVocabulary.fillSet();
		Set<Resource> resources = ShaclVocabulary.collectResourcesFromModel(shapesModel);
		checkTermsUsed(resources);
		checkNamedShapesUsage(shapesModel);
		checkInstanceOfTargetClassExists(shapesModel, instanceModel);
	}

	private static void checkInstanceOfTargetClassExists(Model shapesModel, Model instanceModel)
	{
		RDFNode targetClass = shapesModel.listObjectsOfProperty((Resource) null, SH.targetClass).next();
		if (targetClass == null)
		{
			throw new IllegalStateException("No target class specified.");
		}

		if (!instanceModel.listStatements((Resource) null, RDF.type, targetClass).hasNext())
		{
			throw new IllegalStateException("No instance found of target class: " + targetClass.asResource().getURI());
		}

		System.out.println("Found instance of target class: " + targetClass.asResource().getURI());
	}

	private static void checkNamedShapesUsage(Model model)
	{
		Set<Resource> unreferencedShapeResources = new HashSet<Resource>();
		StmtIterator stmtIterator = model.listStatements((Resource) null,
				ResourceFactory.createProperty(ShaclVocabulary.SHACL_NAMESPACE + "qualifiedValueShape"), (RDFNode) null);
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (!statement.getObject().isURIResource())
			{
				continue;
			}

			StmtIterator shapeIterator = model.listStatements(statement.getResource(), (Property) null, (RDFNode) null);
			if (!shapeIterator.hasNext())
			{
				unreferencedShapeResources.add(statement.getResource());
			}
		}

		if (!unreferencedShapeResources.isEmpty())
		{
			throw new IllegalStateException("Unreferenced shapes found in shape model: " + StringUtils.join(unreferencedShapeResources, ", "));
		}
	}

	private static void checkTermsUsed(Set<Resource> resources)
	{
		List<Resource> resourcesToRemove = new ArrayList<Resource>();
		for (Iterator<Resource> iterator = resources.iterator(); iterator.hasNext();)
		{
			Resource resource = iterator.next();
			if (ShaclVocabulary.shaclTerms.contains(resource.getURI()))
			{
				resourcesToRemove.add(resource);
			}
		}

		resources.removeAll(resourcesToRemove);

		if (!resources.isEmpty())
		{
			throw new IllegalStateException("Unknown shacl terms found in shape model: " + StringUtils.join(resources, ", "));
		}
	}
}
