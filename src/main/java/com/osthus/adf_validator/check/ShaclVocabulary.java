/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.adf_validator.check;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.FileUtils;

public class ShaclVocabulary
{
	public static final String SHACL_NAMESPACE = "http://www.w3.org/ns/shacl#";

	public static Set<String> shaclTerms = new HashSet<String>();

	public static void fillSet() throws FileNotFoundException
	{
		Model shaclVocabulary = ModelFactory.createDefaultModel();
		shaclVocabulary.read(Thread.currentThread().getContextClassLoader().getResourceAsStream("shacl.ttl"), null, FileUtils.langTurtle);
		Set<Resource> shaclResources = collectResourcesFromModel(shaclVocabulary);
		for (Iterator<Resource> iterator = shaclResources.iterator(); iterator.hasNext();)
		{
			Resource resource = iterator.next();
			shaclTerms.add(resource.getURI());
		}
	}

	public static Set<Resource> collectResourcesFromModel(Model model)
	{
		Set<Resource> resources = new HashSet<Resource>();
		StmtIterator stmtIterator = model.listStatements();
		while (stmtIterator.hasNext())
		{
			Statement statement = stmtIterator.next();
			if (statement.getSubject().isURIResource() && statement.getSubject().getURI().startsWith(SHACL_NAMESPACE))
			{
				resources.add(statement.getSubject());
			}
			if (statement.getPredicate().asResource().getURI().startsWith(SHACL_NAMESPACE))
			{
				resources.add(statement.getPredicate().asResource());
			}
			if (statement.getObject().isURIResource() && statement.getResource().getURI().startsWith(SHACL_NAMESPACE))
			{
				resources.add(statement.getResource());
			}
		}
		return resources;
	}

}
