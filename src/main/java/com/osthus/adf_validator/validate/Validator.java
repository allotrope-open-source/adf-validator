/*
 *    Copyright 2017-2018 OSTHUS
 *
 *      http://www.allotrope-framework-architect.com
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.osthus.adf_validator.validate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.FileUtils;
import org.topbraid.shacl.util.ModelPrinter;
import org.topbraid.shacl.validation.ValidationUtil;

import com.osthus.adf_validator.check.ShapesChecker;

public class Validator {
	public static void main(String[] args) throws FileNotFoundException {
		String vocabulary = "vocabulary.ttl";
		String instances = "instances.ttl";
		String shapes = "shapes.ttl";
		if (args.length == 3) {
			vocabulary = args[0];
			instances = args[1];
			shapes = args[2];
		} else if (args.length == 0) {
			System.out.println(
					"Running in no args mode. Looking in current directory for vocabulary.ttl, instances.ttl, and shapes.ttl.");
		} else {
			System.out
					.println("Invalid arguments given. Please enter a vocabulary.ttl, instances.ttl, and shapes.ttl.");
			System.exit(0);
		}
		Model dataModel = ModelFactory.createDefaultModel();
		Model shapesModel = ModelFactory.createDefaultModel();
		dataModel.read(new FileInputStream(Paths.get(instances).toFile()), null, FileUtils.langTurtle);
		dataModel.read(new FileInputStream(Paths.get(vocabulary).toFile()), null, FileUtils.langTurtle);
		shapesModel.read(new FileInputStream(Paths.get(shapes).toFile()), null, FileUtils.langTurtle);
		ShapesChecker.check(shapesModel, dataModel);

		Resource report = ValidationUtil.validateModel(dataModel, shapesModel, true);

		System.out.println(ModelPrinter.get().print(report.getModel()));
		System.out.println("vocabulary: " + vocabulary);
		System.out.println("instances:  " + instances);
		System.out.println("shapes:     " + shapes);
	}
}
